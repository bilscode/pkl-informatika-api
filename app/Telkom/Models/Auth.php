<?php
namespace App\Telkom\Models;

use App\Telkom\Repositories\Auth as Repository;

class Auth
{

    public static function attempt($data = [])
    {
        $attempt = Repository::login($data);
        if ($attempt != 'success') {
            return false;
        }
        $dataByUsername = Repository::getDataByUsername($data['username']);
        return $dataByUsername;
    }

}
