<?php
namespace App\Telkom;

use Illuminate\Support\Facades\Http;

class Request
{

    public const URL = "https://igracias.ittelkom-pwt.ac.id/";

    public static function get($url, $data)
    {
        $server = Request::URL;
        $request = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])
        ->get("{$server}{$url}", $data);
        return $request;
    }

}

