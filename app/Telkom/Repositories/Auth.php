<?php
namespace App\Telkom\Repositories;

use App\Telkom\Request;

class Auth
{

    public static function login($data = [])
    {
        $url = "api/android/ittp_api.php";
        $data['mod'] = 'login2';
        $response = Request::get($url, $data);
        return $response->body();
    }

    public static function getDataByUsername($username)
    {
        $url = "api/android/ittp_api.php";
        $data['mod'] = 'getStudentDatabyUname';
        $data['username'] = $username;
        $response = Request::get($url, $data);
        return $response->json();
    }

}
