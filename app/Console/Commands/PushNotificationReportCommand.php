<?php

namespace App\Console\Commands;

use App\Models\User;
use App\OneSignal\Models\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PushNotificationReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:push-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending reminder to user for submit the report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $not_sent = 0;
        $students = User::student()->onDate()->select('registration_number', 'user_id', 'name')->get();
        $count_student = $students->count();
        if ($count_student > 0) {
            $log1 = 'Sending push notification to '.$count_student.' users ...';
            $log2 = 'Chunking notifications ...';
            $this->info($log1);
            Log::info($log1);
            $this->info($log2);
            Log::info($log2);
            $students_chunked = $students->chunk(20)->toArray();
            $log3 = 'Notification chunked into '.count($students_chunked).' chunk ..';
            $this->info($log3);
            Log::info($log3);
            $this->newLine();
            $this->output->progressStart(count($students_chunked));
            foreach ($students_chunked as $student) {
                $notification = Notification::reminders($student);
                if (!$notification) {
                    $not_sent++;
                }
                $this->output->progressAdvance();
            }
            $this->output->progressFinish();
            if ($not_sent == 0) {
                $log4 = 'Notification sent to '.$count_student.' users ...';
                $this->info($log4);
                Log::info($log4);
            } else {
                $log5 = 'Notification failed to sent to '.$not_sent.' chunks ...';
                $this->error($log5);
                Log::error($log5);
            }
        } else {
            $log6 = 'No user will be sent notification ...';
            $this->error($log6);
            Log::error($log6);
        }
    }
}
