<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;

class ProfileMiddleware
{
    public function handle($request, Closure $next)
    {

        try {
            $auth = request()->header('secret-registration-number');
            if (!$auth) {
                return response()->json([
                    'message' => 'Unauthenticated.'
                ], 401);
            }
            $auth = Crypt::decrypt($auth);
            // Checking Auth to Model
            $check = User::where('registration_number', $auth)->first();
            if (
                is_null($check['work_place_id']) ||
                is_null($check['mentor1_id']) ||
                is_null($check['mentor2_id']) ||
                is_null($check['email']) ||
                is_null($check['telp']) ||
                is_null($check['description']) ||
                is_null($check['date_start']) ||
                is_null($check['date_end'])
            ) {
                return response()->json([
                    'message' => 'Complete your profile first.'
                ], 400);
            }
            return $next($request);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Unauthenticated.'
            ], 401);
        }
    }
}
