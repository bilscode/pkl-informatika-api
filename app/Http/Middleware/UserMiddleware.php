<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;

class UserMiddleware
{
    public function handle($request, Closure $next)
    {
        try {
            $auth = request()->header('secret-registration-number');
            if (!$auth) {
                return response()->json([
                    'message' => 'Unauthenticated.'
                ], 401);
            }
            $auth = Crypt::decrypt($auth);
            // Checking Auth to Model
            $check = User::where('registration_number', $auth)->first();
            if (!$check) {
                return response()->json([
                    'message' => 'Unauthenticated.'
                ], 401);
            }
            if ($check['user_type_id'] != 5) {
                return response()->json([
                    'message' => 'The mobile app for your account is still under development. Sorry, please use the web application.'
                ], 401);
            }
            return $next($request);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Unauthenticated.'
            ], 401);
        }
    }
}
