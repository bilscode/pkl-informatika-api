<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Crypt;

class Controller extends BaseController
{
    public $registration_number;

    public function __construct()
    {
        $auth = request()->header('secret-registration-number');
        $auth = Crypt::decrypt($auth);
        $this->registration_number = $auth;
    }

}
