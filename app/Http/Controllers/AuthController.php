<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\StudyProgram;
use Illuminate\Http\Request;
use App\Telkom\Models\Auth;
use Illuminate\Support\Facades\Crypt;

class AuthController extends Controller
{

    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        $payload = $this->validate($request, $this->loginValidate());
        try {
            //Igracias Auth
            $attempt = Auth::attempt($payload);
            if (!$attempt) {
                return response()->json([
                    'message' => 'Username or password is wrong.'
                ], 401);
            }

            // Lecture Check
            if (!$attempt['prodi']) {
                return response()->json([
                    'message' => 'The mobile app for your account is still under development. Sorry, please use the web application.'
                ], 401);
            }

            // Student Check
            $user = User::where('registration_number', $attempt['nim'])->first();

            if ($user) {
                // If not Student
                if ($user['user_type_id'] != 5) {
                    return response()->json([
                        'message' => 'The mobile app for your account is still under development. Sorry, please use the web application.'
                    ], 401);
                }
            } else {
                //Create user
                $study_program = StudyProgram::where('name', $attempt['prodi'])->first();
                $user = User::create([
                    'user_type_id' => 5,
                    'is_valid_mentor' => true,
                    'registration_number' => $attempt['nim'],
                    'name' => $attempt['fullname'],
                    'study_program_id' => $study_program['study_program_id']
                ]);
            }

            return response()->json([
                'message' => 'Login succes.',
                'data' => $user,
                'meta' => [
                    'token' => Crypt::encrypt($user['registration_number'])
                ]
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }
    }

    public function loginValidate()
    {
        return [
            'username' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'max:255'],
        ];
    }

}
