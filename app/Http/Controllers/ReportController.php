<?php

namespace App\Http\Controllers;

use App\Http\Resources\DailyReportResource;
use App\Models\MasterData;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function daily(Request $request)
    {
        $payload = $this->validate($request, $this->reportDailyValidate());
        $user = User::where('registration_number', $this->registration_number)->first();
        $check_report = $user->daily_reports()->where('date', Carbon::parse($payload['date'])->format('Y-m-d'))->first();
        if ($check_report) {
            $check_report->update([
                'date' => Carbon::parse($payload['date'])->format('Y-m-d'),
                'description' => $payload['description'],
                'is_active' => 1,
                'updatedby' => $user->user_id,
                'is_approve' => 0
            ]);
            $report = $user->daily_reports()->find($check_report->daily_report_id);
        } else {
            $report = $user->daily_reports()->create([
                'user_id' => $user->user_id,
                'date' => Carbon::parse($payload['date'])->format('Y-m-d'),
                'description' => $payload['description'],
                'is_active' => 1,
                'createdby' => $user->user_id,
                'is_approve' => 0
            ]);
        }
        return response()->json([
            'message' => 'Daily Report Created.',
            'data' => new DailyReportResource($report)
        ], 201);
    }

    public function reportDailyValidate()
    {
        $user = User::where('registration_number', $this->registration_number)->first();
        $backdate = Setting::BackDate()->ByStudyProgram($user->study_program_id)->first();
        $allowed_backdate = Carbon::now()->subDays((int) $backdate->description)->format('d-m-Y');
        return [
            'date' => ['required', 'date', 'date_format:d-m-Y', 'before:tomorrow', 'after_or_equal:'.$allowed_backdate],
            'description' => ['required', 'string', 'max:255']
        ];
    }

    public function final(Request $request)
    {
        $payload = $this->validate($request, $this->createFinalReport());
        $user_files = MasterData::where('value', $payload['file_type'])->first();
        $user = User::where('registration_number', $this->registration_number)->first();
        $old_file_name = $user->user_files()->where('user_file_type_id', $user_files['master_data_id'])->first();
        if ($old_file_name) {
            Storage::delete('user_files/'.$user->registration_number.'/'.$old_file_name->name);
        }
        $file = $payload['file'];
        $file_name = $this->registration_number.'_'.$payload['file_type'].'.'.$file->getClientOriginalExtension();
        $file->storeAs('user_files/'.$this->registration_number, $file_name);
        $check_file = $user->user_files()->where('user_file_type_id', $user_files->master_data_id)->first();
        if ($check_file) {
            $check_file->update([
                'description' => $payload['description'],
                'updatedby' => $user->user_id
            ]);
        } else {
            $user->user_files()->create([
                'user_file_type_id' => $user_files->master_data_id,
                'user_id' => $user->user_id,
                'value' => $file_name,
                'name' => $file_name,
                'description' => $payload['description'],
                'is_active' => 1,
                'createdby' => $user->user_id,
            ]);
        }
        return response()->json([
            'message' => 'Report created.',
            'data' => null
        ]);
    }

    public function createFinalReport()
    {
        return [
            'file_type' => ['required', 'string', 'exists:master_data,value,type_data,user_file_type_id'],
            'description' => ['required', 'string', 'max:255'],
            'file' => ['required', 'file', 'mimes:pdf', 'max:4096']
        ];
    }

}
