<?php

namespace App\Http\Controllers;

use App\Http\Resources\DashboardResource;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $user = User::where('registration_number', $this->registration_number)->first();
        return response()->json([
            'message' => 'Dashboard loaded.',
            'data' => new DashboardResource($user)
        ], 200);
    }

}
