<?php

namespace App\Http\Controllers;

use App\Http\Resources\MitraBaseResource;
use App\Models\User;
use App\Models\WorkPlace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class MitraController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $user = User::where('registration_number', $this->registration_number)->first();
        return response()->json([
            'message' => 'Mitra loaded.',
            'data' => new MitraBaseResource($user),
        ], 200);
    }

    public function update(Request $request)
    {
        $payload = $this->validate($request, $this->updateMitra());
        $user = User::where('registration_number', $this->registration_number)->first();
        $payload['date_start'] = Carbon::parse($payload['date_start'])->format('Y-m-d H:i:s');
        $payload['date_end'] = Carbon::parse($payload['date_end'])->format('Y-m-d H:i:s');
        if ($user->work_place_id && $user->mentor2_id && $user->description) {
            $payload = collect($payload)->only(['date_start', 'date_end'])->toArray();
        } else {
            $check_work_place = User::where([
                'user_id' => $payload['mentor2_id'],
                'work_place_id' => $payload['work_place_id']
            ])->first();
            if (!$check_work_place) {
                return response()->json([
                    'mentor2_id' => ['The selected mentor2 id is invalid.']
                ], 200);
            }
        }
        $user->update($payload);
        return response()->json([
            'message' => 'Mitra updated.',
            'data' => true
        ]);
    }

    public function updateMitra()
    {
        return [
            'work_place_id' => ['required', 'numeric', 'exists:work_places,work_place_id'],
            'mentor2_id' => ['required', 'numeric', 'exists:users,user_id,user_type_id,9'],
            'description' => ['required', 'string', 'max:255'],
            'date_start' => ['required', 'date', 'date_format:d-m-Y'],
            'date_end' => ['required', 'date', 'date_format:d-m-Y', 'after:date_start'],
        ];
    }

    public function workPlace(Request $request)
    {
        $payload = $this->validate($request, $this->createWorkPlace());
        $user = User::where('registration_number', $this->registration_number)->first();
        $payload['is_active'] = 1;
        $payload['createdby'] = $user->user_id ;
        $work_place = WorkPlace::create($payload);
        return response()->json([
            'message' => 'Work place created.',
            'data' => $work_place
        ]);
    }

    public function createWorkPlace()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'address' => ['nullable', 'string', 'max:255'],
            'phone_number' => ['nullable', 'string', 'max:20']
        ];
    }

    public function mentor(Request $request)
    {
        $payload = $this->validate($request, $this->createMentor());
        $user = User::where('registration_number', $this->registration_number)->first();
        $reg_number = Carbon::now()->timestamp;
        $data = [
            'user_type_id' => 9,
            'work_place_id' => $payload['work_place_id'],
            'name' => $payload['name'],
            'email' => $payload['email'],
            'telp' => $payload['phone_number'],
            'createdby' => $user->user_id,
            'registration_number' => $reg_number,
            'password' => Hash::make($reg_number)
        ];
        return response()->json([
            'message' => 'Mentor created.',
            'data' => User::create($data)
        ]);
    }

    public function createMentor()
    {
        return [
            'work_place_id' => ['required', 'numeric', 'exists:work_places,work_place_id'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['nullable', 'email', 'max:255'],
            'phone_number' => ['required', 'string', 'max:20']
        ];
    }

}
