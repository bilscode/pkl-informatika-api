<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProfileResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $user = User::where('registration_number', $this->registration_number)->first();
        return response()->json([
            'message' => 'Profile loaded.',
            'data' => new ProfileResource($user)
        ], 200);
    }

    public function update(Request $request)
    {
        $payload = $this->validate($request, $this->updateProfile());
        $user = User::where('registration_number', $this->registration_number)->first();
        if ($payload['photo'] ?? false) {
            $photo = $payload['photo'];
            $name = Str::random(80).'.'.$photo->getClientOriginalExtension();
            $photo->storeAs('users', $name);
            $payload['photo'] = $name;
            Storage::delete('users/'.$user->photo);
        }
        $payload['updated'] = Carbon::now()->format('Y-m-d H:i:s');
        $payload['updatedby'] = $user->user_id;
        $user->update($payload);
        return response()->json([
            'message' => 'Profile updated.',
            'data' => true
        ]);
    }

    public function updateProfile()
    {
        return [
            'telp' => ['required', 'string', 'max:20'],
            'email' => ['required', 'email', 'max:255'],
            'mentor1_id' => ['nullable', 'numeric', 'exists:users,user_id,user_type_id,6'],
            'photo' => ['nullable', 'image', 'mimes:jpg,png,jpeg']
        ];
    }

}
