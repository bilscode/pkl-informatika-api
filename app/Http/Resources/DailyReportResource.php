<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class DailyReportResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'daily_report_id' => $this->daily_report_id,
            'date' => $this->date,
            'description' => $this->description,
            'is_active' => $this->is_active,
            'createdby' => $this->createdby,
            'created' => $this->created,
            'updatedby' => $this->updatedby,
            'updated' => $this->updated,
            'is_approve' => $this->is_approve,
            'approved_by' => User::find($this->updatedby)->name ?? null
        ];
    }
    
}
