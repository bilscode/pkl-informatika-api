<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Announcement;
use App\Models\MasterData;

class DashboardResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'user' => new UserResource($this),
            'mentor_1' => new Mentor1Resource($this->mentor_1),
            'mentor_2' => new Mentor2Resource($this->mentor_2),
            'administrators' => AdministratorResource::collection($this->administrators),
            'daily_reports' => [
                'data' => DailyReportResource::collection($this->daily_reports),
                'count' => $this->daily_reports()->count()
            ],
            'final_reports' => UserFileResource::collection($this->user_files),
            'final_report_setting' => MasterData::where('type_data', 'user_file_type_id')->select('value', 'name')->get(),
            'announcements' => AnnouncementResource::collection(Announcement::programStudy($this->study_program_id)->latest()->get()),
        ];
    }

}
