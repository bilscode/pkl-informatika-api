<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\WorkPlace;

class MitraBaseResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'user' => new UserResource($this),
            'mentor_2' => new Mentor2Resource($this->mentor_2),
            'mitra' => MitraDataResource::collection(WorkPlace::orderBy('name')->get()),
        ];
    }

}
