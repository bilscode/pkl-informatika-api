<?php

namespace App\Http\Resources;

use App\Helpers;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class Mentor1Resource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'user_id' => $this->user_id,
            'user_type_id' => $this->user_type_id,
            'work_place_id' => $this->work_place_id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'is_valid_mentor2' => $this->is_valid_mentor2,
            'value' => $this->value,
            'registration_number' => $this->registration_number,
            'name' => $this->name,
            'gender_id' => $this->gender_id,
            'email' => $this->email,
            'telp' => $this->telp,
            'description' => $this->description,
            'place_birth' => $this->place_birth,
            'study_program_id' => $this->study_program_id,
            'generation_year' => $this->generation_year,
            'photo' => $this->getAttributePhoto($this->photo),
            'is_active' => $this->is_active,
            'createdby' => $this->createdby,
            'created' => $this->created,
            'updatedby' => $this->updatedby,
            'updated' => $this->updated,
            'study_program' => $this->study_program,
            'work_place' => new WorkPlaceResource($this->work_place)
        ];
    }

    public function getAttributePhoto($photo)
    {
        if ($photo) {
            $check = Storage::exists('users/'.$photo);
            if ($check) {
                return url('profile/image/'.$this->user_id);
            }
            $remote = 'https://pkl.informatika.app/assets/media/users/'.$photo;
            $check_remote = Helpers::remote_file_exists($remote);
            if ($check_remote) {
                return 'https://pkl.informatika.app/assets/media/users/'.$photo;
            }
            return url('profile/image/'.$this->user_id);
        }
        return url('profile/image/'.$this->user_id);
    }

}
