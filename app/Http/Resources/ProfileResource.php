<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;

class ProfileResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'user' => new UserResource($this),
            'mentor_1' => $this->mentor_1,
            'mentors' => User::Mentor1()->orderBy('name')->select('user_id', 'name')->get()
        ];
    }

}
