<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MitraDataResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'work_place_id' => $this->work_place_id,
            'name' =>  $this->name,
            'address' =>  $this->address,
            'phone_number' =>  $this->phone_number,
            'is_active' =>  (int) $this->is_active,
            'mentors' => $this->mentors()->select('user_id', 'name')->get()
        ];

    }

}
