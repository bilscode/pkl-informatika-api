<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class AnnouncementResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'announcement_id' => $this->announcement_id,
            'date' => $this->date,
            'name' => $this->name,
            'description' => $this->description,
            'created_by' => User::find($this->createdby)->name
        ];
    }

}
