<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserFileResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'user_file_id' => $this->user_file_id,
            'user_file_type_id' => $this->user_file_type_id,
            'user_id' => $this->user_id,
            'value' => $this->value,
            'name' => $this->name,
            'description' => $this->description,
            'is_active' => $this->is_active,
            'file_type' => $this->master_data->name,
            'url' => $this->getAttributeUrl()
        ];
    }

    public function getAttributeUrl()
    {
        $check = Storage::exists('user_files/'.$this->user->registration_number);
        if ($check) {
            return url('report/'.$this->user_file_type_id.'/'.$this->user->registration_number);
        }
        return 'https://pkl.informatika.app/assets/media/files/'.$this->name;
    }

}
