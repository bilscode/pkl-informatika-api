<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkPlace extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'work_place_id';
    protected $table = 'work_places';

    protected $fillable = [
        'name', 'address', 'phone_number', 'is_active', 'createdby', 'updatedby'
    ];

    public function mentors()
    {
        return $this->hasMany(User::class, 'work_place_id', 'work_place_id')->where('user_type_id', 9)->orderBy('name');
    }
}
