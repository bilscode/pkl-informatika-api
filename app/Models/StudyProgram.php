<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudyProgram extends Model
{

    protected $primaryKey = 'study_program_id';
    protected $table = 'study_programs';

}
