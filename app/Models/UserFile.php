<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFile extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'user_file_id';
    protected $table = 'user_files';

    protected $fillable = [
        'user_file_type_id', 'user_id', 'value', 'name', 'description', 'is_active', 'createdby', 'updatedby'
    ];

    public function master_data()
    {
        return $this->belongsTo(MasterData::class, 'user_file_type_id', 'master_data_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

}
