<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    protected $primaryKey = 'seting_id';
    protected $table = 'settings';

    public function scopeByStudyProgram($query, $study_program_id)
    {
        return $query->where('study_program_id', $study_program_id);
    }

    public function scopeBackDate($query)
    {
        return $query->where('value', 'daily_report_back_date');
    }

}
