<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'score_id';
    protected $table = 'scores';

    protected $fillable = [
        'score_type_id', 'weight', 'value', 'name', 'is_active', 'createdby', 'updatedby'
    ];
}
