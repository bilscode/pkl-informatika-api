<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'announcement_id';
    protected $table = 'announcements';

    public function scopeProgramStudy($query, $program_study_id)
    {
        $programs = [0 , $program_study_id];
        return $query->whereIn('study_program_id', $programs);
    }

    public function created_by()
    {
        return $this->belongsTo(User::class, 'user_id', 'createdby');
    }

}
