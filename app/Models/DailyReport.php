<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyReport extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'daily_report_id';
    protected $table = 'daily_reports';

    protected $fillable = [
        'user_id', 'name', 'date', 'description', 'is_active', 'createdby', 'updatedby', 'is_approve'
    ];

    public function approved_by()
    {
        return $this->belongsTo(User::class, 'updatedby', 'user_id');
    }
}
