<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserScore extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'user_score_id';
    protected $table = 'user_scores';

    protected $fillable = [
        'score_id', 'user_id', 'value', 'createdby', 'updatedby'
    ];

    public function scores()
    {
        return $this->belongsTo(Score::class, 'score_id', 'score_id');
    }
}
