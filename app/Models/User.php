<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'user_id';
    protected $table = 'users';

    protected $fillable = [
        'user_type_id', 'work_place_id', 'date_start', 'date_end', 'mentor1_id', 'mentor2_id', 'is_valid_mentor2', 'value', 'registration_number', 'name', 'gender_id', 'email', 'telp', 'description', 'place_birth', 'study_program_id', 'generation_year', 'photo', 'is_active', 'createdby', 'updatedby', 'password'
    ];

    protected $hidden = ['password'];

    public function work_place()
    {
        return $this->belongsTo(WorkPlace::class, 'work_place_id', 'work_place_id');
    }

    public function daily_reports()
    {
        return $this->hasMany(DailyReport::class, 'user_id', 'user_id')->orderBy('date', 'DESC');
    }

    public function mentor_1()
    {
        try {
            return $this->belongsTo(User::class, 'mentor1_id', 'user_id');
        } catch (\Throwable $th) {
            return null;
        }
    }

    public function mentor_2()
    {
        try {
            return $this->belongsTo(User::class, 'mentor2_id', 'user_id');
        } catch (\Throwable $th) {
            return null;
        }
    }

    public function administrators()
    {
        return $this->hasMany(User::class, 'study_program_id', 'study_program_id')->where('user_type_id', 15);
    }

    public function n1_score_detail()
    {
        try {
            $scores = Score::whereIn('score_type_id', [17,18,19])->pluck('score_id');
            return $this->hasMany(UserScore::class, 'user_id', 'user_id')->whereIn('score_id', $scores)->get();
        } catch (\Throwable $th) {
            return null;
        }
    }

    public function n1_score()
    {
        try {
            $scores = $this->n1_score_detail();
            $count = $scores->count();
            $sum = $scores->sum('value');
            return round($sum/$count, 2);
        } catch (\Throwable $th) {
            return (int) 0;
        }
    }

    public function n2_score_detail()
    {
        try {
            $scores = Score::whereIn('score_type_id', [20])->pluck('score_id');
            return $this->hasMany(UserScore::class, 'user_id', 'user_id')->with('scores')->whereIn('score_id', $scores)->get();
        } catch (\Throwable $th) {
            return null;
        }
    }

    public function n2_score()
    {
        try {
            $score = 0;
            $scores = $this->n2_score_detail();
            foreach ($scores as $loop) {
                $score += ((float)$loop->value*$loop->scores->weight)/100;
            }
            return round($score, 2);
        } catch (\Throwable $th) {
            return (int) 0;
        }
    }

    public function user_files()
    {
        return $this->hasMany(UserFile::class, 'user_id', 'user_id');
    }

    public function study_program()
    {
        return $this->belongsTo(StudyProgram::class, 'study_program_id', 'study_program_id');
    }

    public function scopeMentor1($query)
    {
        return $query->where('user_type_id', 6);
    }

    public function scopeStudent($query)
    {
        return $query->where('user_type_id', 5);
    }

    public function scopeOnDate($query)
    {
        $now = date('Y-m-d');
        return $query->where('date_start', '<=', $now)->where('date_end', '>=', $now);
    }
}
