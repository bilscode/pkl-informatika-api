<?php
namespace App\OneSignal\Models;

use App\OneSignal\Repositories\Notification as Repository;

class Notification
{

    public static function reminders($students = [])
    {
        $filters = [];
        $end =  end($students);
        foreach ($students as $student) {
            $check = $end['user_id'] == $student['user_id'];
            $filters[] = [
                'field' => 'tag',
                'key' => 'registration_number',
                'relation' => '=',
                'value' => $student['registration_number']
            ];
            if (!$check) {
                $filters[] =[
                    'operator' => 'OR',
                ];
            }
        }

        return Repository::create($filters, 'Jangan lupa untuk mengisi laporan harian anda !', 'Laporan Harian');
    }

}
