<?php
namespace App\OneSignal\Repositories;

use App\OneSignal\Request;

class Notification
{

    public static function create($filters, $message, $heading)
    {
        $url = 'notifications';
        $body = [
            'app_id' => env('ONE_SIGNAL_APP_ID'),
            'contents' => [
                'en' => $message
            ],
            'headings' => [
                'en' => $heading
            ],
            'android_group' => 1,
            'filters' => $filters
        ];
        $response = Request::post($url, $body);
        return $response->status();
    }

}
