<?php
namespace App\OneSignal;

use Illuminate\Support\Facades\Http;

class Request
{

    public const URL = "https://onesignal.com/api/v1/";

    public static function post($url, $data)
    {
        $token = env('ONE_SIGNAL');
        $server = Request::URL;
        $request = Http::withHeaders([
            'Authorization' => "Basic {$token}",
            'Content-Type' => 'application/json',
        ])
        ->post("{$server}{$url}", $data);
        return $request;
    }

}

