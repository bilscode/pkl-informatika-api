<?php

use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('login', 'AuthController@login');
});

$router->group(['middleware' => 'student'], function () use ($router) {
    $router->get('profile', 'ProfileController@index');
    $router->post('profile', 'ProfileController@update');


    $router->get('mitra', 'MitraController@index');
    $router->put('mitra', 'MitraController@update');
    $router->post('mitra/work-place', 'MitraController@workPlace');
    $router->post('mitra/mentor', 'MitraController@mentor');

    $router->get('dashboard', 'DashboardController@index');

    $router->group(['middleware' => 'profile'], function () use ($router) {
        $router->post('report/daily', 'ReportController@daily');
        $router->post('report/final', 'ReportController@final');
    });
});

$router->get('profile/image/{user_id}', function ($user_id) {
    $user = User::find($user_id);
    if ($user) {
        try {
            $photo = $user->photo;
            $path = Storage::path('users/'.$photo);
            $file = File::get($path);
            $type = File::mimeType($path);
            return response($file, 200)->header('Content-Type', $type);
        } catch (\Throwable $th) {
            $name = Str::slug($user->name, '+');
            $image = readfile("https://ui-avatars.com/api/?background=E8891B&color=000000&font-size=0.4&size=96&name=$name");
            return response($image, 200)->header('Content-Type', 'image/png');
        }
    } else {
        return response()->json(['message' => 'URL Not Found'],  404);
    }
});

$router->get('report/{file_type}/{registration_number}', function ($file_type, $registration_number) {
    try {
        $user = User::where('registration_number', $registration_number)->first();
        $file = $user->user_files()->where('user_file_type_id', $file_type)->first();
        if ($file) {
            $check = Storage::exists('user_files/'.$registration_number.'/'.$file->name);
            if ($check) {
                return Storage::download('user_files/'.$registration_number.'/'.$file->name, $file->name, [
                    'Content-Type' => 'application/pdf'
                ]);
            }
        }
        return response()->json(['message' => 'URL Not Found'],  404);
    } catch (\Throwable $th) {
        return response()->json(['message' => 'Service Unavailable'],  503);
    }
});
